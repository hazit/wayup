/* task 1 */
const city = "Khmelnytskyi";
const country = "Ukraine";
let population = 265693;
let football_club = true;
console.log("City name is " + city + " located in " + country + " my city population is " + population + " people");

/* task 2 */
let width = 70;
let height = 40;
let res = width * height;
console.log(res);

/* task 3 */
let time = 2;
let speedOfFirst = 95;
let speedOfSecond = 114;
let convergenceRate = speedOfFirst + speedOfSecond;
let result = convergenceRate * time;
console.log(result);

/* task 4 */
/* const randomNumber = Math.floor(Math.random() * 100);
if (randomNumber < 20) {
    console.log("randomNumber меньше 20");
} else if (randomNumber > 50) {
    console.log("randomNumber більше 50");
} else {
    console.log("randomNumber більше 20, і меньше 50");
}
 */
/* task 5 */
const randomNumber2 = Math.floor(Math.random() * 100);
switch (true) {
    case (randomNumber2 < 20):
        console.log("randomNumber меньше 20");
        break;
    case (randomNumber2 > 50):
        console.log("randomNumber більше 50");
        break;
    default:
        console.log("randomNumber більше 20, і меньше 50");
}
