/* task 1 */
for (let i = 10; i <= 50; i++) {
    if (i % 2 == 0) {
        console.log(i);
    }
}

/* task 2 */
const aboutMe = {
    firstName: "Andrew",
    lastName: "Zhytomirskyi",
    age: 27,
    pets: false
};

/* task 3 */
const array = [
    'я в Симбирск,',
    'в трактире.',
    'с утра',
    'В ту же ночь',
    'Я остановился',
    'для закупки',
    'что и было поручено Савельичу.',
    'приехал,',
    'где должен был',
    'нужных вещей',
    'отправился по лавкам',
    'пробыть сутки',
    'Савельич'
]
swap = array[3];
array[3] = array[0];
array[0] = swap;
swap = array[7];
array[7] = array[1];
array[1] = swap;
swap = array[3];
array[3] = array[2];
array[2] = swap;
swap = array[8];
array[8] = array[3];
array[3] = swap;
swap = array[11];
array[11] = array[4];
array[4] = swap;
swap = array[9];
array[9] = array[6];
array[6] = swap;
swap = array[9];
array[9] = array[7];
array[7] = swap;
swap = array[11];
array[11] = array[8];
array[8] = swap;
swap = array[12];
array[12] = array[10];
array[10] = swap;

/* task 4 */
function giveFullName(firstName, lastName) {
    const fullName = `${firstName} ${lastName}`;
    console.log(fullName);
}

/* task 5 */
let i = 27;
while (i <= 67) {
    if (i % 2 != 0) {
        console.log(i);
    }
    i++;
}
